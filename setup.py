# -*- coding: utf-8 -*-
"""
Created on Thu Feb 11 09:17:33 2016

@author: marcobarsacchi
"""

import re
from setuptools import setup


version = re.search(
    '^__version__\s*=\s*"(.*)"',
    open('SwiSpot/SwiSpot.py').read(),
    re.M
    ).group(1)

with open("README.md", "rb") as f:
    long_descr = f.read().decode("utf-8")

setup(
    name = "cmdline-SwiSpot",
    packages = ["SwiSpot"],
    package_data = {'':['config.cfg','README.md']},
    include_package_data =True,
    entry_points = {
        "console_scripts": ['SwiSpot = SwiSpot.SwiSpot:main']
        },
    version=version,
    install_requires = ["numpy","scikit-learn"],
    description = "Command Line SwiSpot Application",
    long_description = long_descr,
    author = "Barsacchi Marco, Bechini Alessio",
    author_email = "barsacchimarco@gmail.com",
    url =""

    )
