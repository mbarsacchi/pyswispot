#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 10 13:55:53 2016

@author: marcobarsacchi

Needed Modules
- Numpy
- Scikit-learn

- ViennaRNA python wrapper
Need an installed version of ViennaRNA [Tested with ViennaRNA 2.1.9]
For the current version of ViennaRNA python interface is compiled and installed
by default.

Typical usage:
python SwiSpot.py sequence [options]

"""

import functionDef as functionF
import argparse
import moduleBP as mbp
import time
import varnaTool
import ConfigParser
import config
import sys
import sdParser

__version__ = "0.2.2"

descr = """Utility for evaluating riboswitches alternative conformations \n
            Barsacchi, Bechini et al.
            ViennaRNA must be installed, as well as its python wrapper.
            """
def main():
    # Is ViennaRNA installed?
    if functionF.subprocess.call("which RNAfold 1>/dev/null",shell=True) ==1:
        raise Exception("ViennaRNA is not installed, or is not in path")

        

    # Defining command line parser
    parser = argparse.ArgumentParser(description=descr)
    parser.add_argument("sequence", help="Sequence to be evaluated using the program")
    parser.add_argument("-c","--conf",default="",help="Provide a path for the conf file")
    parser.add_argument("-d","--distance",default="bp",choices=["bp","md"],help="Distance function for clustering")
    parser.add_argument("-p","--probability", action ="store_true", help ="Uses probability matrix insted of sampled ensemble for switching sequence spotting")
    parser.add_argument("-s","--sampling", choices = ["standard", "wider"], default = "standard", help="Select between standard sampling, at fixed temperature T, and wider sampling")
    parser.add_argument("-t","--temperature", type=float, default = 37.0, help = "Sampling temperature")
    parser.add_argument("-o","--output", help="output data file")
    parser.add_argument("--pattern", default="", help="Provide a path for the pattern file, default pattern are %s and %s" %(config.reg_db[0].replace('y','(').replace('x',')'), config.reg_db[1].replace('y','(').replace('x',')')))
    parser.add_argument("--shinedalgarno",default="", help="Provide a path for the SD sequences file.")
    parser.add_argument("-f","--figures",default ="",choices = ['png','eps','jpeg'], help="Plot figures with varna, using one of the selected formats")
    parser.add_argument("--version", action='version', version='%(prog)s '+__version__)
    parser.add_argument("--verbosity", help="increase output verbosity", action="store_true")


    # Parsing arguments
    args = parser.parse_args()
    t1 = time.time()
    sequence = args.sequence
    if functionF.seqErr(sequence) !=0:
        raise Exception("Not an RNA sequence")

    # Verbosity turned on
    if args.verbosity:
        print "Verbosity turned on"
        print args.temperature
        print "Input Sequence: \n"+sequence

    # Mode selection
    mode = "sampled ensemble"
    if args.probability:
        mode = "probability matrix"

    # Reading configuration file
    if args.conf != "":
        try:
            confp = ConfigParser.ConfigParser()
            confp.read(args.conf)
            config.varna = confp.get('varna','varna')
            config.varna_folder = confp.get('varna','varna_folder')
        except Exception, e:
            print >> sys.stderr, "Error in reading config file '%s'!" %args.conf
            print >> sys.stderr, "Exception: %s" % str(e)
            return
            
    if args.pattern != "":
        try:
            pattern_list = []
            pattern_file = open(args.pattern)
            for line in pattern_file:
                if line.endswith('\n'):
                    pattern_list.append(line[:-2].replace('(','y').replace(')','x'))
                else:
                    pattern_list.append(line.replace('(','y').replace(')','x'))
            config.reg_db = [k for k in pattern_list if k!= '']
        except Exception, e:
            print >> sys.stderr, "Error in reading config file '%s'!" %args.conf
            print >> sys.stderr, "Exception: %s" % str(e)
            return
    
    if args.shinedalgarno != "":
        try:
            config.sd_db = sdParser.load_sd(args.shinedalgarno)
        except Exception, e:
            return
    # Main execution

    # Generate structural ensemble
    print "Generating structural ensemble ....."
    if args.sampling == "standard":
        struct_ensemble = functionF.generate_dot_struct(sequence, T=args.temperature)
    if args.sampling == "wider":
        struct_ensemble = functionF.generate_wider_ensemble(sequence,tmin=int(args.temperature))


    print "Spotting out switching sequences ....."
    # Spotting Switching Sequence
    if args.probability:
        swiSeq, matrix = mbp.scoreExtractionInternal(sequence)
    else:
        swiSeq = functionF.switching_seq(struct_ensemble)

    print "Predicting riboswitch conformations ....."

    predC = functionF.RiboPred(sequence, swiSeq[0], swiSeq[1])

    print "Clustering ensemble ....."
    clust_med,sil_med = functionF.clustering_medoids(struct_ensemble,distance = args.distance)

    print "Evaluating indices ....."
    i_sd = abs(functionF.free_SD(sequence, predC[0][0])-functionF.free_SD(sequence, predC[1][0]))
    i_t = abs(functionF.ter_hairp(sequence, predC[0][0])-functionF.ter_hairp(sequence, predC[1][0]))
    i_sil = sil_med
    itot = i_sd*(1-i_t)+ i_t  +i_sil

    # Plotting figure
    print "Plotting figures ....."
    if args.figures:
        varnaTool.varna_plot(sequence,predC[0][0],'structure1',f_format=args.figures)
        varnaTool.varna_plot(sequence,predC[1][0],'structure2',f_format=args.figures)


    # Print and write to file
    teval = time.time()-t1
    print "Done, evaluated in %f \n" % teval
    print "Conformations: \n"+predC[0][0] +'\n'+predC[1][0]
    print 'SwiSeq : \n%s \n' %str(swiSeq)
    print '%-15s %-15s %-15s %s' % ('Sil: ','SD seq/rel:','Term: ','Tot:')
    print '%-15f %-15f %-15f %f ' % (round(i_sil,3),round(i_sd*(1-i_t),2),round(i_t,2),round(itot,3))

    outFile = args.output
    if outFile:
        with open(outFile,'w') as fOut:
            fOut.write("Mode: %s, sampling: %s, evaluated time: %f seconds \n" %(mode, args.sampling,teval))
            fOut.write("Sequence: \n%s" %sequence +" \nConformation 1: \n%s \nConformation 2: \n%s \n"%(predC[0][0],predC[1][0]))
            fOut.write('SwiSeq : \n%s  \n%-15s %-15s %-15s %s \n%-15f %-15f %-15f %f ' % (str(swiSeq),'Sil: ','SD seq/rel:','Term: ','Tot:',round(i_sil,3),round(i_sd*(1-i_t),2),round(i_t,2),round(itot,3)))
