# -*- coding: utf-8 -*-
"""
Created on Wed Feb 10 16:46:49 2016

@author: marcobarsacchi
"""

# VarnaParse
import subprocess
import config

__rePatt = "([\w].{1,})='(.{1,})'"

def varna_plot(sequence,structure,fname,f_format='eps'):
    __varna__ = config.varna
    __varna_folder__ = config.varna_folder
    command = 'java -cp %s fr.orsay.lri.varna.applications.VARNAcmd -sequenceDBN %s -structureDBN "%s" -o %s -flat false -resolution 3'  %(__varna_folder__+__varna__,sequence,structure,fname+'.'+f_format)
    if subprocess.call(command,shell=True) != 0:
        print "Error in plotting figures. Is varna installed and linked in config.cfg?"
    return
    

