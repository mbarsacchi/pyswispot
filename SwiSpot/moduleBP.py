# -*- coding: utf-8 -*-
"""
Created on Tue Feb  9 10:25:50 2016

@author: marcobarsacchi
"""
import numpy as np
import os
import subprocess
import RNA
import functionDef as fFile
# MODULE RNA_BP_MEASURE
temperature = 37.0
nmin = 4
nmax = 10

def scoreExtractionInternal(seq, weighted=True):
    N = len(seq)
    dPlot = np.zeros([N,N])
    stru = '.'*N
    RNA.free_pf_arrays()
    RNA.init_pf_fold(N)
    struct = RNA.pf_fold(seq,stru)
    bbpm = RNA.export_bppm()

    for i in range(1,N):
        for j in range(i+1,N+1):
            dPlot[i-1,j-1] = RNA.get_pr(i,j)
    mScore = score(dPlot+dPlot.transpose())

    if weighted:
        a = np.arange(nmin,nmax+1)
        wMatrix =  np.matmul(a.reshape(len(a),1),np.ones((1,N-nmin)))
        mScore = mScore/ (wMatrix**1.95)

    maxInd = np.unravel_index(mScore.argmax(),mScore.shape)
    return (maxInd[1],maxInd[0]+nmin),dPlot
        

def score(matrix):
    nSize = matrix.shape[0]
    sp= fFile.sp
    sn = fFile.sn
    mScore = np.zeros((nmax-nmin+1,nSize-nmin))
    for winL in range(nmin,nmax):
        for k in range(nSize-winL):
            pUp = 0
            pDown = 0
            for j in range(k,k+winL):
                for i in range(j):
                    pUp = pUp + matrix[i,j]
                for i in range(j+1,nSize):
                    pDown = pDown+matrix[i,j]
            mScore[winL-nmin,k] = (sp*pUp+sn*pDown)*(sp*pDown+sn*pUp)
            #pUp = np.sum(matrix[:k,k:k+winL])
            #pDown = np.sum(matrix[k+winL:,k:k+winL])
            mScore[winL-nmin,k] = (sp*pUp+sn*pDown)*(sp*pDown+sn*pUp)
    return mScore
