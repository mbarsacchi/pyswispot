# -*- coding: utf-8 -*-
"""
Created on Sat May 28 13:42:42 2016

@author: marcobarsacchi
"""


def load_sd(filePath):
    
    sd_list=[]

    with open(filePath) as fopen:
        for line in fopen:
            if line.endswith('\n'):
                sd_list.append(line[:-2].replace('U','T')+r'.{5,12}?((ATG).{0,30})?$')
            else:
                sd_list.append(line.replace('U','T')+r'.{5,12}?((ATG).{0,30})?$')
    return sd_list

