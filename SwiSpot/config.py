# -*- coding: utf-8 -*-


varna_folder = '/Applications/RNA\ ViewTools/'
varna = 'VARNAv3-93.jar'
reg_db = [r'y{3,}[ACGT]{3,8}x{3,}.T{3,}.{,30}$', r'x{8,}T{3,}.{,30}$' ]
sd_db = ['AGGA.{5,12}?((ATG).{0,30})?$',
 'AGAA.{5,12}?((ATG).{0,30})?$',
 'AAAG.{5,12}?((ATG).{0,30})?$',
 'GGAG.{5,12}?((ATG).{0,30})?$',
 'GAAG.{5,12}?((ATG).{0,30})?$',
 'GGAG.{5,12}?((ATG).{0,30})?$',
 'AAGG.{5,12}?((ATG).{0,30})?$',
 'AGGA.{5,12}?((ATG).{0,30})?$',
 'AAGG.{5,12}?((ATG).{0,30})?$',
 'GAGA.{5,12}?((ATG).{0,30})?$',
 'GTGGA.{5,12}?((ATG).{0,30})?$']