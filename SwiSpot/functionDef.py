# -*- coding: utf-8 -*-
"""
Created on Wed Feb 10 14:02:07 2016

@author: marcobarsacchi
"""
import os
import subprocess
import re
import numpy as np
import RNA
import sklearn.metrics as skm
import k_medoids as km
import config

# Variables
sp = 1.0
sn = 0.0
alphabet = ['A','C','G','T','U']

def seqErr(sequence):
    """
    Function for spotting wrong sequences.
    """
    if type(sequence) != str:
        return 1
    for k in sequence:
        if k.upper() not in alphabet:
            return 1
    return 0
    
    
# Generating DotStruct
def generate_dot_struct(sequence, ensemble=1000, T=37.0):
    """Function for generating ensemble of structures.

    :param sequence: Sequence to analyse.
    :param ensemble: Number of structure to be generated.
    :param T: Sampling temperature.
    :return:
    """

    temperature=''
    if T != 37.0:
        temperature = '--temp=%d' %T

    command_ensemble = """export PATH=$PATH:/usr/local/bin; s=%s; printf "$s" '\n@' | RNAsubopt -p %d %s""" %(sequence, ensemble, temperature)
    structs = os.popen(command_ensemble).read().split('\n')
    structs.remove('')
    struct_ensemble = []
    for k in structs:
        struct_ensemble.append([k, RNA.energy_of_struct(sequence, k)])

    return struct_ensemble


# Generation of Wider Ensemble

# def generate_wider_ensemble(sequence, tmin =37):
#     """
#     Function for generating wider ensembles
#     """
#     
#     ofile = 'toutput.tmp'
#     command = """export PATH=$PATH:/usr/local/bin; s=%s; printf "$s" '\n@' |RNAheat >%s""" %(sequence,ofile)
#     subprocess.call(command,shell=True)
#     tdict = []
#     for klines in open(ofile):
#         sstrip = klines.replace('\n','').split(' ')
#         sstrip = [m for m in sstrip if m != '']
#         tdict.append([int(sstrip[0]),float(sstrip[1])])
#     tdict = tdict[tmin:]
#     tmax = max(tdict,key = lambda x: x[1])[0]
#     tvect = np.linspace(tmin,tmax,11,dtype=int)
#     os.remove(ofile)
#     ense = generate_dot_struct(sequence,ensemble=300, T = tvect[0])
#     for ktemp in tvect[1:7]:
#         ense =ense + generate_dot_struct(sequence,ensemble = 150,T=ktemp)
#     
#     return ense
    
def generate_wider_ensemble(sequence, tmin=37):
    
    RNAopen = os.popen("""export PATH=$PATH:/usr/local/bin; s=%s; printf "$s" '\n@' |RNAheat""" %(sequence)).read().split('\n')
    RNAopen.remove('')
    tdict = []
    for klines in RNAopen:
        sstrip = klines.split(' ')
        sstrip = [m for m in sstrip if m!='']
        tdict.append([int(sstrip[0]), float(sstrip[1])])
    tdict = tdict[tmin:]
    tmax = max(tdict,key = lambda x: x[1])[0]
    tvect = np.linspace(tmin,tmax,11,dtype=int)
    ense = generate_dot_struct(sequence,ensemble=300, T = tvect[0])
    for ktemp in tvect[1:7]:
        ense =ense + generate_dot_struct(sequence,ensemble = 150,T=ktemp)
    
    return ense
    
def score(seq,c):
    s3 = 0
    return sum([sp*sum(eh == c for eh in seq),s3*sum(eh =='.'  for eh in seq),sn*sum(eh!=c and eh!='.' for eh in seq)])
    
def switching_seq(seq_list):
    """
    seq_list = [[struct1, energy1], .... ,[structn, energyn ]]
    
    """
    ## 

    min_finlen = 5
    max_finlen = 10

    maximium = {}
    for finlen in range(min_finlen,max_finlen+1):
        scorea = {i:0 for i in range(len(seq_list[0][0])-finlen)}
        scoreb = {i:0 for i in range(len(seq_list[0][0])-finlen)}
        
        for j in range(len(seq_list[0][0])-finlen):
            for i in range(0,len(seq_list)):
                scorea[j]+= score(seq_list[i][0][j:j+finlen],'(')
                scoreb[j]+= score(seq_list[i][0][j:j+finlen],')')
        v = {}
        for i in range(len(scorea)):
            if np.sign(scorea.values()[i])== -1 or np.sign(scoreb.values()[i])== -1:
                v[-1*abs(scorea.values()[i]*scoreb.values()[i])/(finlen**1.95)] = i
            else:
                v[abs(scorea.values()[i]*scoreb.values()[i])/(finlen**1.95)] = i
        maximium[max(v)] = [v[max(v)],finlen]
    return [maximium[max(maximium)][0],maximium[max(maximium)][1]]
    
    
def RiboPred(sequence,ind,switch_len):
    """ RiboPred(sequence,switch)
        sequence: sequence to fold
        switch: sequence of the switching region
        
        output
        structure1,structure2
        ON state   OFF state
    """
     
    
    constraint1 ='.'*ind+'<'*switch_len+'.'*(len(sequence)-ind-switch_len)
    constraint2 ='.'*ind+'>'*switch_len+'.'*(len(sequence)-ind-switch_len)
    return RNAfold(sequence,constraint1),RNAfold(sequence,constraint2)
    
def RNAfold (sequence,constraint = ''):
    """ Function to get RNAfold
        sequence : sequence to fold,
        constraint: constraint to insert in sequence folded
        The characters " | x < > " are recognized to mark bases that are paired, 
        unpaired, paired upstream, or downstream, respectively. Matching brackets " ( ) " 
        denote base pairs, dots "." are used for unconstrained bases.
    """
    if not constraint:
        return RNA.fold(sequence)
    return [constraint,RNA.fold_par(sequence,constraint,None,1,0)]
    
    
def ter_hairp(sequence,structure):
    """Evaluate terminator hairpins.
    """
    sequence = sequence.replace('U','T')
    se_l = list(sequence)
    st_l = list(structure)
    for i in range(len(sequence)):
        if st_l[i]==')':
            se_l[i] ='x'
        if st_l[i]=='(':
            se_l[i] ='y'
    v = ''.join([k for k in se_l])
    
    for pattern in config.reg_db:
        match = re.search(pattern, v)
        if match:
            return 1
    return 0
    
#    match = re.search(r'y{3,}[ACGT]{3,8}x{3,}.T{3,}.{,30}$',v)
#    if match:
#        return 1
#    else:
#        match = re.search(r'x{8,}T{3,}.{,30}$',v)
#        if match:
#            return 1
#        else:
#            return 0
        
def free_SD(sequence,structure):
    """
    Evaluate SD sequence
    """
    se_l = list(sequence)
    st_l = list(structure)
    for i in range(len(sequence)):
        if st_l[i]==')' or st_l[i]=='(':
            se_l[i] ='x'
    v = ''.join([k for k in se_l])
    for pattern in config.sd_db:
        match = re.search(pattern, sequence)
        if match:
            return sum(k!='x' for k in v[match.start():match.start()+5])/5.0
            
    return -1
#    match = re.search(r'AGGAG.{5,12}?((ATG).{0,30})?$',sequence)
#    if match:
#        return sum(k!='x' for k in v[match.start():match.start()+5])/5.0
#    else:
#        match = re.search(r'AGAAG.{5,12}?((ATG).{0,30})?$',sequence)
#        if match:
#            return sum(k!='x' for k in v[match.start():match.start()+5])/5.0
#        else:
#            match =  re.search(r'AAAGG.{5,12}?((ATG).{0,30})?$',sequence)
#            if match:
#                return sum(k!='x' for k in v[match.start():match.start()+5])/5.0
#            else:
#                match =  re.search(r'GGAGG.{5,12}?((ATG).{0,30})?$',sequence)
#                if match:
#                    return sum(k!='x' for k in v[match.start():match.start()+5])/5.0        
#                else:
#                    match =  re.search(r'GAAGA.{5,12}?((ATG).{0,30})?$',sequence)
#                    if match:
#                        return sum(k!='x' for k in v[match.start():match.start()+5])/5.0
#                    else:
#                        match =  re.search(r'GGAGA.{5,12}?((ATG).{0,30})?$',sequence)
#                        if match:
#                            return sum(k!='x' for k in v[match.start():match.start()+5])/5.0
#                        else:
#                              match =  re.search(r'AAGGT.{5,12}?((ATG).{0,30})?$',sequence)
#                              if match:
#                                  return sum(k!='x' for k in v[match.start():match.start()+5])/5.0
#                              else:
#                                 match =  re.search(r'AGGAA.{5,12}?((ATG).{0,30})?$',sequence)
#                                 if match:
#                                     return sum(k!='x' for k in v[match.start():match.start()+5])/5.0
#                                 else:
#                                     match =  re.search(r'AAGGA.{5,12}?((ATG).{0,30})?$',sequence)
#                                     if match:
#                                         return sum(k!='x' for k in v[match.start():match.start()+5])/5.
#                                     else:
#                                         match =  re.search(r'GAGAA.{5,12}?((ATG).{0,30})?$',sequence)
#                                         if match:
#                                             return sum(k!='x' for k in v[match.start():match.start()+5])/5.0
#                                         else:
#                                             match =  re.search(r'GTGGA.{5,12}?((ATG).{0,30})?$',sequence)
#                                             if match:
#                                                 return sum(k!='x' for k in v[match.start():match.start()+5])/5.0
#                                             else:
#                                                 return -1
#                                                 
    
def clustering_medoids(ensemble, nclust= 2, distance='bp'):
    """Cluster a set of structures using a k-medoids algorithm.
    
    
    Parameters
    ------------
    
    ensemble: set of structures, having the form, 
              ensemble = [['struct1', energy1],...['structn',energyn]]
    nclust :  number of clusters to be created
    
    Returns
    ----------
    labels:   an array, containing cluster label for each sample
    
    silhuette: describing the silhouette for the clustering
    """
    if distance =='bp':
        dist_matrix = matrix_calc_bp(ensemble)
    if distance =='md':
        dist_matrix = matrix_calc_bp(ensemble)
    
    dist_matrix = dist_matrix+dist_matrix.transpose()
    c_data,lv = km.clustermeds(dist_matrix,k=nclust)
    clusters = []
    #for k in c_data[0]:
    for k in c_data:
        if k not in clusters:
            clusters.append(k)
    clust_data = []
    #for v in c_data[0]:
    for v in c_data:
        clust_data.append([e for e,k in enumerate(clusters)if k==v][0])
    silhouette = skm.silhouette_score(dist_matrix,np.array(clust_data),metric='precomputed') 
    return clust_data, silhouette


# DISTANCE FUNCTION EVALS

def matrix_calc_bp(ensemble):
    """
    Evaluate distance matrix on the ensemble
    """
    l = len(ensemble)
    dist_matrix= np.zeros([l,l])
    for k in range(l):
        for j in range(k-1):
            dist_matrix[k,j] = RNA.bp_distance(ensemble[k][0],ensemble[j][0])
    return dist_matrix
    
def matrix_calc_md(struct_ens):
    l = len(struct_ens)
    dist_matrix = np.zeros([l,l])
    for k in range(l):
        for j in range(k+1):
            dist_matrix[k,j] = float(m_distance(struct_ens[k][0],struct_ens[j][0]))
    return dist_matrix
    

def dot_to_mountain(dot_string):
    l = len(dot_string)
    m_notation = [dot_string[:i].count('(') - dot_string[:i].count(')') for i in range(1,l+1)]
    return m_notation

def m_distance(s1, s2, p=1):
    m1 = dot_to_mountain(s1)
    m2 = dot_to_mountain(s2)
    v = [abs(e[0]-e[1])**p for e in zip(m1, m2)]
    v = sum(v)**1.0/p
    return v
    
    
    

