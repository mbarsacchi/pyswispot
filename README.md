# SwiSpot

SwiSpot is a command line utility for predicting riboswitch alternative
configurations. It is based upon the prediction of the so-called switching
sequence, to subsequently constrain the folding of the two functional structures.
While theoretically able to find any kind of switching behavior, under the
assumption of sufficient representation in the ensemble of structures,
currently SwiSpot is able to classify only riboswitches that rely on
transcription termination via terminator hairpins, or translation inhibition
via SD sequestering/releasing.


[1] Barsacchi M, Novoa EM, Kellis M, Bechini A. SwiSpot: modeling riboswitches by 
spotting out switching sequences. _Bioinformatics_. 2016 Nov 1;32(21):3252-3259.
Epub 2016 Jul 4. PubMed PMID: 27378291. DOI:[10.1093/bioinformatics/btw401](10.1093/bioinformatics/btw401)


## Python dependencies


Needed Modules

- Numpy
- Scikit-learn
- ViennaRNA python wrapper [Will be installed along as ViennaRNA]


## Non-python dependencies

An installed version of ViennaRNA [Tested with ViennaRNA 2.1.9] is required.
For the current version of ViennaRNA python interface is compiled and installed
by default.
ViennaRNA can be installed from [Vienna TBI](http://www.tbi.univie.ac.at/RNA/)
```
 tar -zxvf ViennaRNA-2.1.9.tar.gz
 cd ViennaRNA-2.1.9
 ./configure
 make
 sudo make install
```

Mac OS X user can install ViennaRNA using homebrew:
```
brew install ViennaRNA
```
---
Finally, figure plotting requires the VARNA java applet, as indicated
in the conf file.


## Installation


SwiSpot can be easily installed using the provided setup.py.
The required libraries will be installed by the package.
An installed and linked ViennaRNA, with python wrapper is needed!

 Unpack cmdline-SwiSpot-x.y.z.tar.gz
```    
tar -zxvf cmdline-SwiSpot-x.y.z.tar.gz
```
 Enter directory  
```    
cd cmdline-SwiSpot-x.y.z.tar.gz
```
 Install
```    
python setup.py install
```


## Usage:
     SwiSpot [-h] [-c CONF] [-d {bp,md}] [-p] [-s {standard,wider}]
                   [-t TEMPERATURE] [-o OUTPUT] [--pattern PATTERN]
                   [--shinedalgarno SHINEDALGARNO] [-f {png,eps,jpeg}]
                   [--version] [--verbosity]
                   sequence

                  positional arguments:
                    sequence              Sequence to be evaluated using the program

                  optional arguments:
                    -h, --help            show this help message and exit
                    -c CONF, --conf CONF  Provide a path for the conf file
                    -d {bp,md}, --distance {bp,md}
                                          Distance function for clustering
                    -p, --probability     Uses probability matrix insted of sampled ensemble for
                                          switching sequence spotting
                    -s {standard,wider}, --sampling {standard,wider}
                                          Select between standard sampling, at fixed temperature
                                          T, and wider sampling
                    -t TEMPERATURE, --temperature TEMPERATURE
                                          Sampling temperature
                    -o OUTPUT, --output OUTPUT
                                          output data file
                    --pattern PATTERN     Provide a path for the pattern file, default pattern
                                          are ({3,}[ACGT]{3,8}){3,}.T{3,}.{,30}$ and
                                          ){8,}T{3,}.{,30}$
                    --shinedalgarno SHINEDALGARNO
                                          Provide a path for the SD sequences file.
                    -f {png,eps,jpeg}, --figures {png,eps,jpeg}
                                          Plot figures with varna
                    --version             show program's version number and exit
                    --verbosity           increase output verbosity

### Common options


## Pattern file and Shine-Dalgarno sequences file

Users can provide their own pattern for terminator hairpins, using ```--pattern FILEPATH'''
(a plain text file one for each row), (see pattern.txt for an example). 
Moreover users can provide their own Shine-Dalgarno sequences via a plain text
file, using the command ```--shinedalgarno FILEPATH''', 
(shine-dalgarno.txt for an example.)

## Configuration file

Configuration file , provides information this program.
Moreover, if plotting is enabled, varna must be located somewhere, and its folder
as well as its name must be specified in config.cfg.
Users can provide their own config.cfg with
```
SwiSpot -c /Path/to/conf/config.cfg
```

**config.cfg** default:
```
[varna]
varna_folder = /Applications/RNA\ ViewTools/
varna = VARNAv3-93.jar

```


## Changelog
### 0.2.2
Possibility of introduce personalized terminator regex or shine-delgarno sequences.
### 0.2.1
No more writing to disk during operation.
### 0.2.0
Evaluation of probability matrix using ViennaRNA python Wrapper

## TODO

- [ ] Various clustering algorithms, using scikit-learn
- [ ] More metrics (string-edit distance, tree-edit distance,...)
